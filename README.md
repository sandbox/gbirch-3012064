Select or Other User Options is an adjunct to the Select or Other module (select_or_other).

The Select or Other module contains a setting that permits a user to add "Other"
options to the list of available options on the fly.  Unfortunately, this means that every user
on the system is presented with any new option.  On a system with many users, this can be 
problematic, at best.

Select or Other User Options permits users to add options on the fly and stores those new
options in user data, so they are presented to the user, but no one else.

To use the module, install and enable as any other module.  Once enabled, any field for which
the Select or Other widget is specified will have a new setting: "Other value as user-specific 
option." To permit users to save their own options, choose whether to save the new options for 
all instances of the field (for cases where a single field is re-used for many content types)
or just for that particular instance of the field.  If the field is only used in one content
type, it does not matter which you select.

If you have enabled user options for a field, you should change the display settings of the field to 
user the "Select or other User Options" formatter, rather than "Select or Other".  Failure to do this 
will give you unexpected results if either a) the option value contains HTML entities or b) you decide to 
change the option label.

There is a rudimentary form for altering custom user options that may be found as a new tab on 
the user page "Edit Custom Options".  From here, option labels may be changed, and options may
be deleted.

For cases where you want more than one user to share a single set of custom options, the module
implements 'hook_soo_user_options_user' so that you can, for instance, store and retrieve all 
custom options for an organic group in and from the user data of the owner of the group.
