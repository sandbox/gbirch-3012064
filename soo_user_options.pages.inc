<?php

/**
 * @file Form for editing a user's custom options.
 */

/**
 * Custom options edit form.
 *
 * /user/%user/user-options.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 * @param \stdClass $account
 *   The user selected for editing.
 *
 * @return array
 *   Form array.
 */
function soo_user_options_edit_form(array $form, array &$form_state, \stdClass $account) {
  // During initial form build, add the entity to the form state for use during
  // form building and processing. During a rebuild, use what is in the form
  // state.
  if (!isset($form_state['user'])) {
    $form_state['user'] = soo_get_user([], $account);
  }
  else {
    $account = $form_state['user'];
  }
  $user_options = soo_get_user_options($account);
  if (empty($user_options)) {
    $form = [
      'message' => [
        '#markup' => t('There are no custom options for this account.'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ],
    ];
    return $form;
  }

  $form['#user'] = $account;
  // Force the form processor to maintain the nested form structure.
  $form['#tree'] = TRUE;

  $form['description'] = [
    '#markup' => t('Please note that this form can only update your list of options and does not change values stored in the database.  If you delete an option, existing data will show up in the "other" field.'),
    '#prefix' => '<h2>' . t('Update custom options') . '</h2><p>',
    '#suffix' => '</p>',
  ];

  $field_map = array_intersect_key(field_info_field_map(), $user_options);
  $form_fields = [];
  $fieldset_titles = [];
  $paragraphs_info = entity_get_info('paragraphs_item');

  // Sort the field by bundle and label.
  foreach ($user_options as $field_name => &$field_data) {
    $form_fields[$field_name] = [
      '#type' => 'fieldset',
    ];
    $field_titles = [];
    // Default options.
    if (!empty($field_data['options'])) {
      // Set up container.
      $form_fields[$field_name]['default'] = [
        '#type' => 'container',
        'label' => ['#markup' => '<p class="description">' . t('Default Options - leave option value blank to delete') . '</p>'],
      ];
      // Add fields for each option.
      foreach ($field_data['options'] as $option_key => $option_text) {
        $form_fields[$field_name]['default'][$option_key] = [
          '#type' => 'textfield',
          '#default_value' => $option_text,
        ];
      }
      foreach ($field_map[$field_name]['bundles'] as $field_entity => $bundles) {
        foreach ($bundles as $bundle) {
          $instance = field_info_instance($field_entity, $field_name, $bundle);
          if (!empty($instance['label'])) {
            $field_titles[] = $instance['label'];
          }
        }
      }
    }
    unset($field_entity, $bundle);

    // Bundle-specific options.
    $keys = array_keys($field_data);
    $field_entities = array_diff($keys, ['options']);
    foreach ($field_entities as $field_entity) {
      foreach ($field_data[$field_entity] as $bundle => $options) {
        if (!empty($options)) {
          $instance = field_info_instance($field_entity, $field_name, $bundle);
          if (!empty($instance['label'])) {
            $label = $field_titles[] = $instance['label'];
          }
          $label = isset($label) ? $label : $field_name;
          if ('node' === $field_entity) {
            $label .= ' (' . node_type_get_name($bundle) . ')';
          }
          elseif ('paragraphs_item' === $field_entity && isset($paragraphs_info['bundles'][$bundle])) {
            $label .= ' (' . $paragraphs_info['bundles'][$bundle]['label'] . ')';
          }
          $form_fields[$field_name][$field_entity][$bundle] = [
            '#type' => 'container',
            'label' => ['#markup' => '<p class="description">' . $label . ' - ' . t('leave option value blank to delete') . '</p>'],
          ];
          // Add options.
          foreach ($options as $option_key => $option_text) {
            $form_fields[$field_name][$field_entity][$bundle][$option_key] = [
              '#type' => 'textfield',
              '#default_value' => $option_text,
            ];
          }
        }
      }
    }

    // Title for the master fieldset.  Record title for sorting.
    $field_title = implode(' / ', array_unique($field_titles));
    $fieldset_titles[] = $field_title;
    $form_fields[$field_name]['#title'] = $field_title;

  }
  // Sort the fieldsets by title.
  array_multisort($fieldset_titles, SORT_ASC, SORT_NATURAL, $form_fields);
  $form['user_options'] = $form_fields;
  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  return $form;

}

/**
 * Submission function for custom options edit form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 */
function soo_user_options_edit_form_submit(array $form, array &$form_state) {
  $option_values =& $form_state['values']['user_options'];
  $account = $form_state['user'];
  foreach ($option_values as $field_name => $field_options) {
    foreach ($field_options as $field_entity => $entity_data) {
      if ('default' == $field_entity) {
        soo_set_user_options(soo_user_options_clean_options($entity_data), $field_name, NULL, NULL, $account, FALSE);
      }
      else {
        foreach ($entity_data as $bundle => $bundle_options) {
          soo_set_user_options(soo_user_options_clean_options($bundle_options), $field_name, $bundle, $field_entity, $account, FALSE);
        }
      }
    }
  }
  try {
    user_save($account);
  }
  catch (\Exception $e) {
    watchdog_exception(
      'Select or Other User Options',
      $e,
      'Unable to save revised user options for %user',
      ['%user' => $account->name . '(' . $account->uid . ')']);
  }

  drupal_set_message(t('Revised user options have been saved.'));
}

/**
 * Utility function to clean up a set of custom user options.
 *
 * Runs soo_user_options_clean_value on each option.
 *
 * @param array $options
 *   The new option values entered on the form.
 *
 * @return array
 *   List of non-empty options.
 */
function soo_user_options_clean_options(array $options) {
  if (empty($options)) {
    return [];
  }
  array_walk($options, function (&$val) {
    $val = soo_user_options_clean_value($val);
  });
  return array_filter($options);
}
