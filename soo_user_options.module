<?php

/**
 * @file soo_user_options.module
 *
 * Hook implementations for the soo_user_options module.
 */

/**
 * Implements hook_menu().
 */
function soo_user_options_menu() {
  $items = [
    'user/%user/user-options' => [
      'title' => 'Edit Custom Options',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['soo_user_options_edit_form', 1],
      'access callback' => 'user_edit_access',
      'access arguments' => [1],
      'type' => MENU_LOCAL_TASK,
      'file' => 'soo_user_options.pages.inc',
    ],
  ];
  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function soo_user_options_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  // Only act if this is a select or other widget.
  if ('select_or_other' === $form['#instance']['widget']['module']) {
    $user_options_settings = [
      '#type' => 'select',
      '#title' => t('<em>Other</em> value as user-specific option'),
      '#description' => t("If any incoming default values do not appear in <em>available options</em> should they be saved as a user-specific option?"),
      '#options' => [
        'all' => t('Save as user option for all instances of this field'),
        'instance' => t('Save as user option for this instance only'),
      ],
      '#empty_option' => '-' . t('Do not save as user option') . '-',
      '#default_value' => (isset($form['#instance']['widget']['settings']['soo_user_options']) ? $form['#instance']['widget']['settings']['soo_user_options'] : ''),
      '#required' => FALSE,
    ];
    // Insert the control where we want it.
    $widget_settings = $form['instance']['widget']['settings'];
    $form['instance']['widget']['settings'] = [];
    foreach ($widget_settings as $key => $setting) {
      $form['instance']['widget']['settings'][$key] = $setting;
      if ('other_unknown_defaults' === $key) {
        $form['instance']['widget']['settings']['soo_user_options'] = $user_options_settings;
      }
    }
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function soo_user_options_field_widget_select_or_other_form_alter(&$element, &$form_state, $context) {
  soo_user_options_alter_select_widget($element, $context);
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function soo_user_options_field_widget_select_or_other_buttons_form_alter(&$element, &$form_state, $context) {
  soo_user_options_alter_select_widget($element, $context);
}

/**
 * Alters the select or other widget to add user options and extra validation.
 *
 * @param array &$element
 *   The select widget.
 * @param array &$context
 *   Context about the field.
 */
function soo_user_options_alter_select_widget(array &$element, array &$context) {
  if (!empty($context['instance']['widget']['settings']['soo_user_options'])) {
    $element['#options'] = soo_user_options_prepare_options($context, $element['#options']);
    $element['#element_validate'][] = 'soo_user_options_widget_validate';
  }
}

/**
 * Adds user options to the list of available options.
 *
 * @param array $context
 *   Context about the field.
 * @param array $options
 *   Array of field options.
 *
 * @return array
 *   Array of options for the form element.
 */
function soo_user_options_prepare_options(array $context, array $options) {

  $soo_user_option = $context['instance']['widget']['settings']['soo_user_options'];
  $instance = &$context['instance'];
  $settings = &$instance['widget']['settings'];

  // Need to extract field_name.
  $field_name = $instance['field_name'];
  $list = [];
  if ($soo_user_option == 'all') {
    $list = soo_get_user_options(NULL, $field_name);
  }
  elseif ($soo_user_option == 'instance') {
    $bundle = $instance['bundle'];
    $entity_type = $instance['entity_type'];
    $list = soo_get_user_options(NULL, $field_name, $bundle, $entity_type);
  }

  // Copied from select_or_other_field_widget_form_prepare_options().
  // Does not support option groups.
  if (!empty($list)) {
    foreach ($list as $key => $opt) {
      // select_or_other ignores keys except in specific circumstances.
      select_or_other_field_widget_form_prepare_option($options, NULL, $key . '|' . $opt, $settings);
    }
    // Re-sort the options if asked.
    if (isset($settings['sort_options']) && $settings['sort_options']) {
      natcasesort($options);
    }
  }

  return $options;
}

/**
 * Saves an "other" option to user's private list of options.
 *
 * Will throw error if the option contains html.
 *
 * @param array $element
 *   The form element.
 * @param array &$form_state
 *   Form state.
 */
function soo_user_options_widget_validate(array $element, array &$form_state) {

  // This function is only called if the soo_user_options setting is not empty.
  if (
    ($element['select']['#value'] == 'select_or_other' || (is_array($element['select']['#value']) && isset($element['select']['#value']['select_or_other'])))
    && !empty($element['other']['#value'])
  ) {
    // Sanitize the entered value without encoding.
    $value = trim($element['other']['#value']);
    $clean_value = soo_user_options_clean_value($value);
    if ($value !== $clean_value) {
      form_error(
        $element['other'],
        t(
          '!name: The value may not include html.',
          ['!name' => t($element['select']['#title'])])
      );
      return;
    }
    // The key cannot have any html entities in it, or all hell breaks loose.
    $key = soo_user_options_clean_key($value);
    // Test whether this other value is already in the options.
    if (array_key_exists($value, $element['#options'])
      || array_key_exists($key, $element['#options'])
      || in_array($value, $element['#options'])
      || in_array($key, $element['#options'])) {
      return;
    }

    $field_name = $element['#field_name'];
    if (!empty($form_state['field'][$field_name][$element['#parents'][1]]['instance'])) {
      $instance = $form_state['field'][$field_name][$element['#parents'][1]]['instance'];
    }
    else {
      $instance = field_widget_instance($element, $form_state);
    }
    $soo_user_option = $instance['widget']['settings']['soo_user_options'];
    if ('instance' === $soo_user_option) {
      soo_set_user_option($key, $value, $field_name, $instance['bundle'], $instance['entity_type']);
    }
    elseif ('all' === $soo_user_option) {
      soo_set_user_option($key, $value, $field_name);
    }

    // Store the key.  See select_or_other_field_widget_validate().
    form_set_value($element, [['value' => $key]], $form_state);
  }
}

/**
 * Trims and removes html tags from an option label.
 *
 * @param string $value
 *   The option label as entered by the user.
 *
 * @return string
 *   A trimmed string without html tags.
 */
function soo_user_options_clean_value($value) {
  return strip_tags(trim($value));
}

/**
 * Creates a key that contains nothing to be munged by filters or encoding.
 *
 * @param string $value
 *   The string from which the key should be created.
 *
 * @return string
 *   A key suitable for use as an option key by the select_or_other module.
 */
function soo_user_options_clean_key($value) {
  // Most common non-ascii replacement.
  $key = str_replace('&', 'and', $value);
  // Replace non-ascii characters with ''.
  $key = preg_replace('/[^A-Za-z0-9\. -;:]/', '', $key);
  return $key;
}

/**
 * Implements hook_field_formatter_info().
 */
function soo_user_options_field_formatter_info() {
  return [
    'soo_user_options_formatter' => [
      'label' => t('Select or other User Options'),
      'field types' => ['text'],
    ],
  ];
}

/**
 * Implements hook_field_formatter_view().
 *
 * Mostly this is just a copy of select_or_other_field_formatter_view().
 */
function soo_user_options_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = [];
  $field_options = [];

  if (isset($instance['widget']['settings']['available_options'])) {
    $field_options = explode("\n", $instance['widget']['settings']['available_options']);
    $pos = strpos($instance['widget']['settings']['available_options'], '|');

    if ($pos !== FALSE) {
      // There are keys.
      foreach ($field_options as $field_item) {
        $exploded = explode('|', $field_item);
        $temp_options[$exploded[0]] = isset($exploded[1]) ? $exploded[1] : $exploded[0];
      }
      $field_options = $temp_options;
    }
    $entity_info = entity_get_info($entity_type);
    $bundle_property = $entity_info['entity keys']['bundle'];
    $field_options += soo_get_user_options(NULL, $field['field_name'], $entity->$bundle_property, $entity_type);
  }

  foreach ($items as $delta => $item) {
    if (array_key_exists($item['value'], $field_options)) {
      $element[$delta] = ['#markup' => check_plain($field_options[$item['value']])];
    }
    else {
      $element[$delta] = ['#markup' => isset($item['safe_value']) ? $item['safe_value'] : check_plain($item['value'])];
    }
  }

  return $element;
}

/**
 * Returns the user account to retrieve options from/store options to.
 *
 * Invokes hook_soo_user_options_user_alter().
 *
 * @param array $context
 *   Array of information about the field whose options are requested.
 * @param null|\stdClass $account
 *   Optional. Defaults to current logged in user. May be overridden by hook.
 *
 * @return \stdClass
 *   A user object.  Normally this is a clone of the current user object.
 */
function soo_get_user(array $context = [], $account = NULL) {
  if (!$account || !is_object($account) || !isset($account->uid)) {
    global $user;
    $account = clone $user;
  }
  else {
    // Signal to hook implementations that a specific user was requested.
    $context['specified_account'] = TRUE;
  }
  drupal_alter('soo_user_options_user', $account, $context);
  return $account;
}

/**
 * Returns array of user options.
 *
 * @param string|null $field_name
 *   The name of the field for which options are wanted.
 * @param string|null $bundle
 *   The entity bundle for which options are wanted. Defaults to NULL for all.
 * @param string $entity_type
 *   The entity type for which optoins are wanted.  Defaults to 'node'.  Ignored
 *   if bundle is not specified.
 *
 * @return array
 *   Returns an empty array if none found.
 */
function soo_get_user_options($account = NULL, $field_name = NULL, $bundle = NULL, $entity_type = 'node') {
  $context = [
    'field_name' => $field_name,
    'bundle' => $bundle,
    'entity_type' => $entity_type,
  ];
  if (!isset($account) || !is_object($account) || !isset($account->uid)) {
    $account = soo_get_user($context);
  }

  if (isset($account->data['soo_user_options'])) {
    $option_sets = $account->data['soo_user_options'];
  }
  else {
    return [];
  }

  if (!$field_name) {
    return $option_sets;
  }

  if (!isset($option_sets[$field_name])) {
    return [];
  }

  // Return the specific if available.
  if ($bundle && isset($option_sets[$field_name][$entity_type][$bundle])) {
    return $option_sets[$field_name][$entity_type][$bundle];
  }

  return (isset($option_sets[$field_name]['options']) ? $option_sets[$field_name]['options'] : []);

}

/**
 * Sets the custom user options for a field.
 *
 * @param array $options
 *   Array of key => value pairs.
 * @param string $field_name
 *   Machine name of field.
 * @param null|string $bundle
 *   Machine name of the bundle for which to save the options, if any.
 * @param string $entity_type
 *   Detaults to "node"; ignored if $bundle is not specified.
 * @param null|\stdClass $account
 *   Defaults to results of soo_get_user().
 * @param bool $save
 *   Whether to save the user, defaults to true.
 */
function soo_set_user_options(array $options, $field_name, $bundle = NULL, $entity_type = 'node', $account = NULL, $save = TRUE) {
  $context = [
    'field_name' => $field_name,
    'bundle' => $bundle,
    'entity_type' => $entity_type,
  ];
  if (!isset($account) || !is_object($account) || !isset($account->uid)) {
    $account = soo_get_user($context);
  }
  if (!isset($account->data['soo_user_options'])) {
    $account->data['soo_user_options'] = [];
  }
  if (!isset($account->data['soo_user_options'][$field_name])) {
    $account->data['soo_user_options'][$field_name] = [];
  }
  if ($bundle) {
    if (empty($options)) {
      unset($account->data['soo_user_options'][$field_name][$entity_type]);
    }
    else {
      $account->data['soo_user_options'][$field_name][$entity_type][$bundle] = $options;
    }
  }
  else {
    if (empty($options)) {
      unset($account->data['soo_user_options'][$field_name]['options']);
    }
    else {
      $account->data['soo_user_options'][$field_name]['options'] = $options;
    }
  }
  // Cleanup.
  if (empty($options) && empty($account->data['soo_user_options'][$field_name])) {
    unset($account->data['soo_user_options'][$field_name]);
  }
  // Note: this should update the loaded account.
  if ($save) {
    try {
      user_save($account);
    }
    catch (\Exception $e) {
      watchdog_exception(
        'Select or Other User Options',
        $e,
        'Unable to save user options for %user',
        ['%user' => $account->name . '(' . $account->uid . ')']);
    }
  }
}

/**
 * Sets a custom user option for a field.
 *
 * Can be used to edit the value of, or delete existing options.
 *
 * @param string $option_key
 *   The option key.  Value that will be stored in the database.
 * @param string $option_value
 *   The option label.
 * @param string $field_name
 *   Machine name of field.
 * @param null|string $bundle
 *   Machine name of the bundle for which to save the options, if any.
 * @param string $entity_type
 *   Detaults to "node"; ignored if $bundle is not specified.
 * @param bool $save
 *   Whether to save the user, defaults to true.
 */
function soo_set_user_option($option_key, $option_value, $field_name, $bundle = NULL, $entity_type = 'node', $account = NULL, $save = TRUE) {
  $options = soo_get_user_options($account, $field_name, $bundle, $entity_type);
  if (is_null($option_value)) {
    unset($options[$option_key]);
  }
  else {
    $options[$option_key] = $option_value;
  }
  soo_set_user_options($options, $field_name, $bundle, $entity_type, $account, $save);
}
